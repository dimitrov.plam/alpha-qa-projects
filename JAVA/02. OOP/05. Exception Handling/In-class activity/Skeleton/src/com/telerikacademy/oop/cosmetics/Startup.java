package com.telerikacademy.oop.cosmetics;

import com.telerikacademy.oop.cosmetics.core.EngineImpl;

public class Startup {

    public static void main(String[] args) {
        EngineImpl engine = new EngineImpl();
        engine.start();
    }
}
