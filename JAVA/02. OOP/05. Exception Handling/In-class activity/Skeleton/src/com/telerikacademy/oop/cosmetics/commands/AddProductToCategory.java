package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;

import java.util.List;

public class AddProductToCategory implements Command {
    private static final String PRODUCT_ADDED_TO_CATEGORY = "Product %s added to category %s!";

    private final ProductRepository productRepository;
    private String result;

    public AddProductToCategory(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void execute(List<String> parameters) {
        //TODO Validate parameters count

        String categoryNameToAdd = parameters.get(0);
        String productNameToAdd = parameters.get(1);

        result = addProductToCategory(categoryNameToAdd, productNameToAdd);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String addProductToCategory(String categoryName, String productName) {
        //TODO Validate product and category exist

        Category category = productRepository.getCategories().get(categoryName);
        Product product = productRepository.getProducts().get(productName);

        category.addProduct(product);

        return String.format(PRODUCT_ADDED_TO_CATEGORY, productName, categoryName);
    }
}
