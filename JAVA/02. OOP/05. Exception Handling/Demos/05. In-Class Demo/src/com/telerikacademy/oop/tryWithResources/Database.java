package com.telerikacademy.oop.tryWithResources;

import java.io.IOException;
import java.util.List;

public class Database implements AutoCloseable {

    private String connection = null;

    public Database() {
        openConnection();
    }

    public List<Person> retrieveUsers() {
        return List.of(new Person(2));
    }

    private void openConnection() {
        System.out.println("Opening connection to the database");
        this.connection = "www.learn.telerikacademy.com/a25-java";
    }

    public void closeConnection() {
        this.connection = null;
        System.out.println("Closing the connection to the database.");
    }

    @Override
    public void close() {
        closeConnection();
    }
}
