package com.telerikacademy.oop.tryWithResources;

public class Person implements Employable {
    private int age;

    public Person(int age) {
        this.age = age;
    }

    @Override
    public void work() {
        System.out.println("I'm working!");
    }
}
