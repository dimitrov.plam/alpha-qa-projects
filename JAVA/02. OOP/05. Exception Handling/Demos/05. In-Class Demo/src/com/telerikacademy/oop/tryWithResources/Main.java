package com.telerikacademy.oop.tryWithResources;

public class Main {
    public static void main(String[] args) {
        EmployeeDepartment employeeDepartment = new EmployeeDepartment();
        employeeDepartment.raiseSalaryOfEveryone();
    }
}
