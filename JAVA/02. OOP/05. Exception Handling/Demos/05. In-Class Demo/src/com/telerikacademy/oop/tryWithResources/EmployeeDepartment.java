package com.telerikacademy.oop.tryWithResources;

import java.util.List;

public class EmployeeDepartment {

    private List<Person> employees;

    public EmployeeDepartment() {
//        Database database = new Database();
//        try {
//            employees = database.retrieveUsers();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            database.closeConnection();
//        }

        try (Database database = new Database()) {
            employees = database.retrieveUsers();
        }

    }

    public void raiseSalaryOfEveryone() {
        System.out.println("$$$$$$");
    }


}
