package com.telerikacademy.oop.exceptions;

import com.telerikacademy.oop.noTryWithResources.BackupInputProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        readUserInput();
    }

    public static void readUserInput() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int input = 0;
        try {
            input = Integer.parseInt(reader.readLine());
//            doSomeVeryImportantWork(); -> will cause an Error (StackOverflowError)
        } catch (NumberFormatException e) {
            System.out.println("exception while trying to parse the input - invalid data type; set input to the default value");
            input = 1;
            throw new NullPointerException();
        } catch (NullPointerException npe) {
            System.out.println("npe exception while trying to use the reader - provide a backup reader");
            Scanner scanner = new Scanner(System.in);
            input = scanner.nextInt(); // ?
        } catch (IOException e) {
            System.out.println("exception while trying to receive input");
            input = BackupInputProvider.getBackupInput();
        } finally {
            System.out.println("im in finally");
            if (input == 0) {
                System.out.println("??? cannot divide by 0");
            } else {
                System.out.println("Result: " + (100 / input));
            }
        }

    }

    public static void doSomeVeryImportantWork() {
        doSomeVeryImportantWork();
    }

}
