package com.telerikacademy.oop.noTryWithResources;

import java.util.List;

public class Database {

    private String connection = null;

    public Database() {

    }

    public List<Person> retrieveUsers() {
        return List.of(new Person(2));
    }

    public void openConnection() {
        System.out.println("Opening connection to the database");
        connection = "www.learn.telerikacademy.com/a25-java";
    }

    public void closeConnection() {
        connection = null;
        System.out.println("Closing the connection to the database.");
    }

}
