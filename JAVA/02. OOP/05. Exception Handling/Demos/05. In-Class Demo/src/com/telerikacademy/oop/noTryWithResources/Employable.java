package com.telerikacademy.oop.noTryWithResources;

public interface Employable {
    void work();
}
