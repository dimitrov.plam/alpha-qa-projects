package com.telerikacademy.oop.noTryWithResources;

import java.util.List;

public class EmployeeDepartment {

    private List<Person> employees;

    public EmployeeDepartment() {
        Database database = new Database();
        database.openConnection();
        employees = database.retrieveUsers();
        database.closeConnection();
    }


}
