package com.telerikacademy.oop.tryWithResources;

public interface Employable {
    void work();
}
