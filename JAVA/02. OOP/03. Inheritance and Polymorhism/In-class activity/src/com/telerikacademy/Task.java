package com.telerikacademy;

import java.time.LocalDate;

public class Task extends BoardItem{

    private final Status initialStatus = Status.TODO;
    private final Status finalStatus = Status.VERIFIED;
    private  final static int MIN_ASSIGNEE_LENGTH = 5;
    private  final static int MAX_ASSIGNEE_LENGTH = 30;
    private  final static String ASSIGNEE_ERROR_MESSAGE = String.format("Assignee's name must be between %s and %s chars",
                                                                        MIN_ASSIGNEE_LENGTH, MAX_ASSIGNEE_LENGTH);

    private String assignee;

    public Task(String title, String assignee, LocalDate dueDate) {
        super(title, dueDate);
        validateAssignee(assignee);
        this.assignee = assignee;
        this.status= initialStatus;
    }

    public String getAssignee() {
        return assignee;
    }

    private void setAssignee(String assignee) {
        validateAssignee(assignee);

        logEvent(String.format("Assignee changed from %s to %s", getAssignee(), assignee));

        this.assignee = assignee;
    }

    private void validateAssignee(String assignee) {
        if (assignee == null) {
            throw new IllegalArgumentException("Assignee cannot be null!");
        }
        if (assignee.length() <MIN_ASSIGNEE_LENGTH | assignee.length() > MAX_ASSIGNEE_LENGTH ) {
            throw new IllegalArgumentException(ASSIGNEE_ERROR_MESSAGE);
        }
    }
    @Override
    public void revertStatus() {
        if (status != initialStatus) {
            setStatus(Status.values()[status.ordinal() - 1]);
        } else {
            logEvent(String.format("Can't revert, already at %s", getStatus()));
        }
    }


}
