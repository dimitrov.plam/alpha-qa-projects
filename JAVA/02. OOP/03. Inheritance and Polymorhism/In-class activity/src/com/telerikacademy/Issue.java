package com.telerikacademy;

import java.time.LocalDate;
import java.util.Objects;

public class Issue extends BoardItem{

    private final String description;
    private final static String NULL_DESCRIPTION = "No description";

    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate);
        this.description = (description == null ? NULL_DESCRIPTION : description);
    }

    public String getDescription() {
        return description;
    }



}
