package com.telerikacademy;

import com.telerikacademy.loggers.ConsoleLogger;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        LocalDate tomorrow = LocalDate.now().plusDays(1);

        BoardItem task = new Task("Write unit tests", "Pesho", tomorrow);
        BoardItem issue = new Issue("Review tests", "Someone must review Pesho's tests.", tomorrow);

        Board.addItem(task);
        Board.addItem(issue);


        ConsoleLogger logger = new ConsoleLogger();
        Board.displayHistory(logger);

    }

}
