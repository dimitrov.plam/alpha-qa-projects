package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private final static List<BoardItem> items = new ArrayList<>();

    private Board() {
    }

    public static void addItem(BoardItem item) {
        if (items.contains(item)) {
            throw new IllegalArgumentException("Item already in the list");
        }

        items.add(item);
    }

    public static int totalItems() {
        return items.size();
    }

    public static void displayHistory() {
        for (BoardItem item :
                items) {
            item.displayHistory();
        }
    }

}
