package com.telerik;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {

        BoardItem item1 = new BoardItem("Implement login/logout", LocalDate.now().plusDays(3));
        BoardItem item2 = new BoardItem("Secure admin endpoints", LocalDate.now().plusDays(5));

        Board.addItem(item1); // add item1
        Board.addItem(item2); // add item2
        System.out.println(Board.totalItems());
        Board.addItem(item1); // do nothing - item1 already in the list
        Board.addItem(item2); // do nothing - item2 already in the list

        System.out.println(Board.totalItems()); // count: 2


    }

}
