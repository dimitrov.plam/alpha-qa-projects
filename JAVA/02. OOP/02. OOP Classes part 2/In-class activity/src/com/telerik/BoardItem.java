package com.telerik;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BoardItem {
    private final Status initialStatus = Status.OPEN;
    private final Status finalStatus = Status.VERIFIED;

    private static final int MIN_TITLE_LENGTH = 5;
    private static final int MAX_TITLE_LENGTH = 30;

    private String title;
    private LocalDate dueDate;
    private Status status;

    public List<EventLog>  events;
//    public EventLog event;

    public BoardItem(String title, LocalDate dueDate){
        this(title, dueDate, Status.values()[0]);


    }

    public BoardItem(String title, LocalDate dueDate, Status status){

        this.events = new ArrayList<>();
        setTitle(title);
        //this.title = title;
        this.dueDate= dueDate;
        this.status = status;

        this.addEvent(String.format("Item created: '%s' , [%s | %s]",getTitle(),getStatus(),getDueDate()));

    }
    public String getTitle() {
        return title;
    }

    private void setTitle(String title) {
        if (title == null){
            throw new IllegalArgumentException("Title can't be null");
        }
        if (title.length()< MIN_TITLE_LENGTH || title.length() > MAX_TITLE_LENGTH){
            throw new IllegalArgumentException("Title name must be between 5 and 30 characters");
        }
        String oldTitle = getTitle();
        this.title = title;
        addEvent(String.format("Title changed from '%s' to '%s'",oldTitle, getTitle()));
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    private void setDueDate(LocalDate dueDate) {
        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("Due date can't be in the past");
        }
        LocalDate oldDueDate = getDueDate();
        this.dueDate = dueDate;
        addEvent((String.format("DueDate changed from %s to %s", oldDueDate, getDueDate())));


    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void revertStatus(){
        if (this.status != initialStatus){
            Status oldstatus = getStatus();
            status = Status.values()[status.ordinal() - 1];
            addEvent(String.format("Status changed from %s to %s",oldstatus.toString(), getStatus().toString()));
        }
        else
            addEvent("Can't revert already at Open");
    }

    public  void advanceStatus(){
        if (this.status != finalStatus){
            Status oldstatus = getStatus();
            status = Status.values()[status.ordinal() + 1];
            addEvent(String.format("Status changed from %s to %s",oldstatus.toString(), getStatus().toString()));
        }
        else addEvent("Can't advance already at Verified");
    }

    public void addEvent(String description){
        if (description == null){
            throw new IllegalArgumentException("Description cannot be null!");
        }
        events.add(new EventLog(description));

    }

    public String viewInfo(){
        return String.format("'%s', [%s | %s]", title, status, dueDate);
    }

    public String displayHistory(){
        StringBuilder sb = new StringBuilder();
        for (EventLog event :
                events) {
            sb.append(event.viewInfo()).append(System.getProperty("line.separator"));
        }
        System.out.println(sb.toString());
        return sb.toString();
    }

    public static void main(String[] args) {
        BoardItem item = new BoardItem("Refactor this mess", LocalDate.now().plusDays(2));
        item.setDueDate(item.getDueDate().plusYears(2));
        item.setTitle("Not that important");
        item.revertStatus();
        item.advanceStatus();
        item.revertStatus();

        item.displayHistory();

        System.out.println("\n--------------\n");

        BoardItem anotherItem = new BoardItem("Don't refactor anything",  LocalDate.now().plusDays(10));
        anotherItem.advanceStatus();
        anotherItem.advanceStatus();
        anotherItem.advanceStatus();
        anotherItem.advanceStatus();
        anotherItem.advanceStatus();
        anotherItem.displayHistory();



    }

}

