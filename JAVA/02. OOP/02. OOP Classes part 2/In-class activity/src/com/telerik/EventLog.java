package com.telerik;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class EventLog {
    private final String description;
    private final LocalDateTime timestamp;

    public EventLog(String description){
        if (description == null ){
            throw new IllegalArgumentException("Description cannot be null");
        }
        this.description = description;
        this.timestamp = LocalDateTime.now();
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }
    public  String viewInfo(){
        String  formattedDateTime = timestamp.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm:ss"));
        return String.format("[%s] %s", formattedDateTime, getDescription());
    }


}
