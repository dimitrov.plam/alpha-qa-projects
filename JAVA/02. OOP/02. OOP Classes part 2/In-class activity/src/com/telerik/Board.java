package com.telerik;

import java.util.ArrayList;
import java.time.LocalDate;
import java.util.List;

public class Board {
    private  static List<BoardItem> items = new ArrayList<>();

    private Board(){

    }
    public static void addItem(BoardItem item){
        if (Board.items.contains(item)){
            throw new IllegalArgumentException("Item already in the list");
        }
        Board.items.add(item);
    }
    public static int totalItems(){
        return Board.items.size();
    }
}
