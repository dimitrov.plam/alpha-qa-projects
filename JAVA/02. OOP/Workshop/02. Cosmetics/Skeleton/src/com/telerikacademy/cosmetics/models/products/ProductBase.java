package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public  class ProductBase implements Product{
    //Finish the class
    //implement proper interface (see contracts package)
    //validate

    private static final int NAME_MIN_LENGTH = 3;
    private static final int NAME_MAX_LENGTH = 10;
    private static final String NAME_INVALID_MESSAGE = String.format("Name should be between %d and %d symbols.",
                                                                        NAME_MIN_LENGTH,NAME_MAX_LENGTH);
    private static final int BRAND_MIN_LENGTH = 2;
    private static final int BRAND_MAX_LENGTH = 10;
    private static final String BRAND_INVALID_MESSAGE = String.format("Brand should be between %d and %d symbols.",
                                                                        BRAND_MIN_LENGTH,BRAND_MAX_LENGTH);
    private static final String PRICE_INVALID_MESSAGE = "Price should be non negative.";



    String name;
    String brand;
    double price;
    GenderType gender;
    
    ProductBase(String name, String brand, double price, GenderType gender) {
        //throw new NotImplementedException();
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    @Override
    public String getName() {
        return name;
    }
    private void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null!");
        }
        if (name.length()< NAME_MIN_LENGTH || name.length() > NAME_MAX_LENGTH) {
            throw new IllegalArgumentException(NAME_INVALID_MESSAGE);
        }
        this.name = name;
    }

    @Override
    public String getBrand() {
        return brand;
    }
    private void setBrand(String brand) {
        if (brand == null) {
            throw new IllegalArgumentException("Brand cannot be null!");
        }
        if (brand.length() < BRAND_MIN_LENGTH || brand.length() > BRAND_MAX_LENGTH) {
            throw new IllegalArgumentException(BRAND_INVALID_MESSAGE);
        }
        this.brand = brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException(PRICE_INVALID_MESSAGE);
        }
        this.price = price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }


    @Override
    public String print() {
        return String.format(" #%s %s \n #Price: $%.2f \n #Gender: %s",
                                getName(), getBrand(), getPrice(), getGender().toString());
    }
}
