package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste {

    private List<String> ingredients= new ArrayList<>();
    
    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);
        setIngredients(ingredients);
    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(ingredients);
    }

    private void setIngredients(List<String> ingredients) {
        if (ingredients == null) {
            throw new IllegalArgumentException("Ingredients List cannot be null!");
        }
        this.ingredients = ingredients;
    }

    public String print1() {
        return String.format(" # %s %s \n #Price: %2f \n #Gender: %s \n #Ingredients: [%s] \n ===",
                                    getName(), getBrand(), getPrice(), getGender().toString(),
                                    String.join(",",ingredients));
    }
    @Override
    public String print() {
        return String.format(super.print() + "\n #Ingredients: [%s] \n ===", String.join(",",ingredients));

    }

}
