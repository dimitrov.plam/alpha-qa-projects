package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.products.ProductBase;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {
    //use constants for validations values

    //I use the Category constrains from previous task
    private static final int MIN_CATEGORY_LENGTH = 2;
    private static final int MAX_CATEGORY_LENGTH = 15;
    private static final String NAME_INVALID_MESSAGE = String.format("Category name must be between %d and %d characters",
                                                                        MIN_CATEGORY_LENGTH, MAX_CATEGORY_LENGTH);
    
    private String name;
    private List<Product> products;
    
    public CategoryImpl(String name) {
        //validate
        //initialize the collection
        setName(name);
        products = new ArrayList<>();
    }
    
    public String getName() {
        return name;
    }

    private  void setName(String name) {
        if (name == null){
            throw new IllegalArgumentException("Category name cannot be null!");
        }
        if (name.length() <MIN_CATEGORY_LENGTH || name.length() > MAX_CATEGORY_LENGTH) {
            throw new IllegalArgumentException(NAME_INVALID_MESSAGE);
        }
        this.name = name;
    }
    
    public List<Product> getProducts() {
        //todo why are we returning a copy? Replace this comment with explanation!
        /* for better Encapsulation we return a copy of the list, so the list is less vulnerable*/
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {
        if (product == null){
            throw new IllegalArgumentException("Product cannot be null");
        }
        products.add(product);
    }
    
    public void removeProduct(Product product) {
        if (product == null){
            throw new IllegalArgumentException("Product cannot be null");
        }
        if (getProducts().contains(product)){
            products.remove(product);
        }
        else throw new IllegalArgumentException("This product is not found in this Category");
    }

    
    //The engine calls this method to print your category! You should not rename it!
    public String print() {
        StringBuilder result = new StringBuilder();
        result.append(String.format("#Category: %s\n", name));
        if (products.size() == 0) {
           result.append(" #No product in this category");
        }
        for (Product product : products) {
            result.append(product.print());
        }
        return result.toString();
        //finish ProductBase class before implementing this method
        

    }
    
}
