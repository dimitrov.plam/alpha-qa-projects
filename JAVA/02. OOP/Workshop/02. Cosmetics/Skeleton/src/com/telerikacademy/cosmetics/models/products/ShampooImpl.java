package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;

public class ShampooImpl extends ProductBase implements Shampoo {

    int milliliters;
    UsageType usageType;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usageType) {
        super(name,brand,price,gender);
        setMilliliters(milliliters);
        this.usageType = usageType;

    }


    @Override
    public int getMilliliters() {
        return milliliters;
    }

    private void setMilliliters(int milliliters) {
        if (milliliters < 0) {
            throw new IllegalArgumentException("Milliliters cannot be less than zero!");
        }
        this.milliliters = milliliters;
    }


    @Override
    public UsageType getUsage() {
        return usageType;
    }

    @Override
    public String print(){
        return String.format(super.print() + "\n #Milliliters: %d \n #Usage: %s \n ===",
                                getMilliliters(),getUsage().toString());
    }
}
