package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleBase implements Vehicle {

    private final static String TYPE_CANT_BE_NULL = "Vehicle type cannot be null!";

    // data for validating generic passenger capacity
    private final int VEHICLE_MIN_PASSENGERS =1;
    private final int VEHICLE_MAX_PASSENGERS = 800;
    private final String VEHICLE_PASSENGERS_ERROR_MESSAGE =
                    String.format("A vehicle with less than %d passengers or more than %d passengers cannot exist!",
                    VEHICLE_MIN_PASSENGERS, VEHICLE_MAX_PASSENGERS);

    // data for validating price per kilometer
    private final double VEHICLE_MIN_PRICE_PER_KILOMETER =0.10;
    private final double VEHICLE_MAX_PRICE_PER_KILOMETER = 2.50;
    private final String VEHICLE_PRICE_PER_KILOMETER_ERROR_MESSAGE =
            String.format("A vehicle with a price per kilometer lower than %.2f or higher than %.2f cannot exist!",
                    VEHICLE_MIN_PRICE_PER_KILOMETER,VEHICLE_MAX_PRICE_PER_KILOMETER);


    protected int passengerCapacity;
    protected double pricePerKilometer;
    private final VehicleType type;

    //constructor
    public VehicleBase(int passengerCapacity, double pricePerKilometer,VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        if (type == null)
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        this.type = type;
    }





    @Override
    public int getPassengerCapacity() {
        return this.passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return this.pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return this.type;
    }

    protected void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < VEHICLE_MIN_PASSENGERS | passengerCapacity > VEHICLE_MAX_PASSENGERS) {
            throw new IllegalArgumentException(VEHICLE_PASSENGERS_ERROR_MESSAGE);
        }
        this.passengerCapacity = passengerCapacity;
    }

    public void setPricePerKilometer(double pricePerKilometer) {
        if (pricePerKilometer < VEHICLE_MIN_PRICE_PER_KILOMETER | pricePerKilometer > VEHICLE_MAX_PRICE_PER_KILOMETER) {
            throw new IllegalArgumentException(VEHICLE_PRICE_PER_KILOMETER_ERROR_MESSAGE);
        }
        this.pricePerKilometer = pricePerKilometer;
    }


    @Override
    public String print() {
        return String.format("Passenger capacity: %d\n" +
                             "Price per kilometer: %.2f\n" +
                             "Vehicle type: %s",
                             getPassengerCapacity(), pricePerKilometer, type);
    }

    @Override
    public String toString(){
    return this.print();
   }

    public String printClassName(){
        return getClass().getSimpleName().replace("Impl", "");
    }

}
