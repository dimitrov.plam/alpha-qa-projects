package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    //validation data for cart capacity
    private final static int MIN_CART_AMOUNT = 1;
    private final static int MAX_CART_AMOUNT = 15;
    private  final static String CART_AMOUNT_ERROR =
            String.format("A train cannot have less than %d cart or more than %d carts.",
                    MIN_CART_AMOUNT, MAX_CART_AMOUNT);

    //validation data for train's passenger capacity
    private final static int MIN_TRAIN_CAPACITY = 30;
    private final static int MAX_TRAIN_CAPACITY = 150;
    private final static String TRAIN_CAPACITY_ERROR =
            String.format("A train cannot have less than %d passengers or more than %d passengers.",
                    MIN_TRAIN_CAPACITY, MAX_TRAIN_CAPACITY);


    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setCarts(carts);
    }

    @Override
    public int getCarts() {
        return this.carts;
    }

    private void setCarts(int carts) {
        if (carts < MIN_CART_AMOUNT | carts > MAX_CART_AMOUNT) {
            throw new IllegalArgumentException(CART_AMOUNT_ERROR);
        }
        this.carts = carts;
    }

    @Override
    protected void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_TRAIN_CAPACITY | passengerCapacity > MAX_TRAIN_CAPACITY) {
            throw new IllegalArgumentException(TRAIN_CAPACITY_ERROR);
        }
        this.passengerCapacity = super.getPassengerCapacity();
    }

    @Override
    public String print(){
        return String.format("%s ----\n" +
                             "%s\n" +
                             "Carts amount: %d\n",
                             printClassName(), super.print(), carts);
    }

}
