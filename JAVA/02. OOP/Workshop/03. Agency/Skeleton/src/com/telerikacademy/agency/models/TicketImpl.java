package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {

    private Journey journey;
    private double administrativeCosts;
    
    public TicketImpl(Journey journey, double administrativeCosts) {
        this.journey = journey;
        this.administrativeCosts = administrativeCosts;
    }



    @Override
    public double getAdministrativeCosts() {
        return this.administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return this.journey;
    }

    @Override
    public double calculatePrice() {
        return (getAdministrativeCosts() * journey.calculateTravelCosts());
    }

    @Override
    public String print() {

        return String.format("Ticket ----\nDestination: %s\nPrice: %.2f\n", journey.getDestination(),calculatePrice());
    }

    @Override
    public  String toString() {
        return this.print();
    }
}
