package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {

    private boolean hasFreeFood;

    //constructor
    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer, VehicleType.AIR);
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public boolean hasFreeFood() {
        return this.hasFreeFood;
    }

    @Override
    public String print(){
        return String.format("%s ----\n" +
                             "%s\n" +
                             "Has free food: %s\n",
                             printClassName(), super.print(), hasFreeFood);
    }

}
