package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    //validation data for bus's passenger capacity
    private final static int MIN_BUS_CAPACITY = 10;
    private final static int MAX_BUS_CAPACITY = 50;
    private final static String BUS_CAPACITY_ERROR =
            String.format("A bus cannot have less than %d passengers or more than %d passengers.",
                                    MIN_BUS_CAPACITY, MAX_BUS_CAPACITY);

    //constructor
    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
    }

    @Override
    protected void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_BUS_CAPACITY || passengerCapacity > MAX_BUS_CAPACITY) {
            throw new IllegalArgumentException(BUS_CAPACITY_ERROR);
        }
        this.passengerCapacity = super.getPassengerCapacity();
    }


    @Override
    public String print(){
        return String.format("%s ----\n" +
                             "%s\n",
                             printClassName(), super.print());
    }
}
