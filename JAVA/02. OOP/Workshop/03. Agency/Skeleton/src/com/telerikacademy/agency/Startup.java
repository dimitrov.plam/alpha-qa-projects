package com.telerikacademy.agency;

import com.telerikacademy.agency.core.EngineImpl;
import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.BusImpl;
import com.telerikacademy.agency.models.vehicles.VehicleBase;

public class Startup {
    
    public static void main(String[] args) {

        EngineImpl engine = new EngineImpl();
        engine.start();
    }
    
}
