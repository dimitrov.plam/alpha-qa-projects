package com.telerikacademy.oop.cosmetics.models.products;

import com.telerikacademy.oop.cosmetics.models.common.GenderType;

public class Product {

    private final int MIN_PRODUCT_LENGTH = 3;
    private final int MIN_BRAND_LENGTH = 2;
    private final int MAX_PRODUCT_LENGTH = 10;
    private final int MAX_BRAND_LENGTH = 10;
    
    private double price;
    private String name;
    private String brand;
    private GenderType gender;
    
    public Product(String name, String brand, double price, GenderType gender) {
        // finish the constructor and validate data
        setName(name);
        setPrice(price);
        setBrand(brand);
        setGender(gender);

    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if (price <= 0.00){
            throw new IllegalArgumentException("Price must be higher than zero");
        }
        this.price = price;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null){
            throw new IllegalArgumentException("Product name cannot be null");
        }
        if (name.length() < MIN_PRODUCT_LENGTH ||name.length() > MAX_PRODUCT_LENGTH){
            throw new IllegalArgumentException("Product name must be between 3 and 10 characters");
        }
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        if (brand == null){
            throw new IllegalArgumentException("Brand name cannot be null");
        }
        if (brand.length() < MIN_BRAND_LENGTH || brand.length() > MAX_BRAND_LENGTH){
            throw new IllegalArgumentException("Brand name must be between 2 and 10 characters");
        }
        this.brand = brand;
    }

    public GenderType getGender() {
        return gender;
    }

    private void setGender(GenderType gender) {
        if (gender == null){
            throw new IllegalArgumentException("Gender Type  cannot be null");
        }
        this.gender = gender;
    }

    public String print() {
        return String.format(" # %s %s \n #Price: %f \n #Gender: %s", getName(), getBrand(), getPrice(), getGender().toString());
        // Format:
        //" #[Name] [Brand]
        // #Price: [Price]
        // #Gender: [Gender]
        // ==="
    }
    
}
