package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private final int MIN_CATEGORY_LENGTH = 2;
    private final int MAX_CATEGORY_LENGTH = 15;

    private String name;
    private List<Product> products;
    
    public Category(String name) {
        setName(name);
        products = new ArrayList<Product>();
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null){
            throw new IllegalArgumentException("Category name cannot be null");
        }
        if (name.length() < MIN_CATEGORY_LENGTH || name.length() > MAX_CATEGORY_LENGTH){
            throw new IllegalArgumentException("Category name must be between 2 and 15 characters");
        }
        this.name = name;
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {
        if (product == null){
            throw new IllegalArgumentException("Product cannot be null");
        }
        products.add(product);
    }
    
    public void removeProduct(Product product) {
        if (product == null){
            throw new IllegalArgumentException("Product cannot be null");
        }
        if (getProducts().contains(product)){
            products.remove(product);
        }
        else throw new IllegalArgumentException("This product is not found in this Category");
    }
    
    public String print() {
      StringBuilder sb = new StringBuilder();
      sb.append(String.format("#Category: %s",getName()));
        if (products.size() >0){
            sb.append(System.getProperty("line.separator"));
            for (Product product :
                    products) {
                sb.append(product.print()).append(System.getProperty("line.separator"));
                sb.append(" ===");
            }
        }
        else{
            sb.append(System.getProperty("line.separator")).append(" #No product in this category");
        }
        return sb.toString();
    }
    
}
