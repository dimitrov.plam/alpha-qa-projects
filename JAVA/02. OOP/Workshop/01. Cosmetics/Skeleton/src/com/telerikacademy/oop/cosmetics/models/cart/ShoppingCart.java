package com.telerikacademy.oop.cosmetics.models.cart;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    
    private List<Product> productList;
    
    public ShoppingCart() {
        productList = new ArrayList<Product>();
    }


    
    public List<Product> getProductList() {
        return new ArrayList<>(productList);
    }
    
    public void addProduct(Product product) {
        if (product == null){
            throw new IllegalArgumentException("Product cannot be null");
        }
        productList.add(product);
    }
    
    public void removeProduct(Product product) {
        if (product == null){
            throw new IllegalArgumentException("Product cannot be null");
        }
        productList.remove(product);
    }
    
    public boolean containsProduct(Product product) {
        if (product == null){
            throw new IllegalArgumentException("Product cannot be null");
        }
        return getProductList().contains(product);
    }
    
    public double totalPrice() {
        double sum = 0.00;
        for (Product product :
                getProductList()) {
            sum  += product.getPrice();
        }
        return sum;
        //throw new UnsupportedOperationException("Not implemented yet.");
    }
    
}
