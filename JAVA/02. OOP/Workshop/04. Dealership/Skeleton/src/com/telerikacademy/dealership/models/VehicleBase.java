package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {
    
    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";
    
    //add fields
    private String make;
    private String model;
    private double price;
    private final VehicleType vehicleType;
    private final List<Comment> comments;
    
    // finish the constructor and validate input; look in package com.telerikacademy.dealership.models.common.enums; what methods are there in VehicleType?
    VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        setMake(make);
        setModel(model);
        setPrice(price);

        if (vehicleType == null){
            throw new IllegalArgumentException("VehicleType can't be null!");
        }
        this.vehicleType = vehicleType;

        comments = new ArrayList<>();
    }
    


    @Override
    public String getMake() { return make; }

    @Override
    public String getModel() { return model; }

    @Override
    public double getPrice() { return price; }

    @Override
    public VehicleType getType() { return vehicleType; }

    @Override
    public int getWheels() { return vehicleType.getWheelsFromType(); }

    @Override
    public List<Comment> getComments() { return new ArrayList<>(comments); }

    @Override
    public void removeComment(Comment comment) { comments.remove(comment); }

    @Override
    public void addComment(Comment comment) { comments.add(comment); }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        //finish implementation of toString() - what should the next 3 lines of code append to the builder?
        builder.append(String.format("  %s: %s",MAKE_FIELD, make)).append(System.lineSeparator());
        builder.append(String.format("  %s: %s", MODEL_FIELD, model)).append(System.lineSeparator());
        builder.append(String.format("  %s: %d", WHEELS_FIELD, getWheels())).append(System.lineSeparator());

        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());

        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }

    //In the meaning of encapsulation, this method is set to protected so only derived classes to have access to it.
    protected abstract String printAdditionalInfo();

    private void setMake(String make) {
        Validator.ValidateNull(make,String.format(ModelsConstants.FIELD_CANNOT_BE_NULL,MAKE_FIELD));

        Validator.ValidateIntRange(make.length(),ModelsConstants.MIN_MAKE_LENGTH,ModelsConstants.MAX_MAKE_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,
                        MAKE_FIELD, ModelsConstants.MIN_MAKE_LENGTH, ModelsConstants.MAX_MAKE_LENGTH));

        this.make = make;
    }

    private void setModel(String model) {
        Validator.ValidateNull(model, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL, MODEL_FIELD));

        Validator.ValidateIntRange(model.length(), ModelsConstants.MIN_MODEL_LENGTH, ModelsConstants.MAX_MODEL_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,
                        MODEL_FIELD, ModelsConstants.MIN_MODEL_LENGTH, ModelsConstants.MAX_MODEL_LENGTH));

        this.model = model;
    }

    private void setPrice(double price) {
        Validator.ValidateDecimalRange(price, ModelsConstants.MIN_PRICE, ModelsConstants.MAX_PRICE,
                String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                PRICE_FIELD, ModelsConstants.MIN_PRICE, ModelsConstants.MAX_PRICE));

        this.price = price;
    }

    private String printComments() {
        StringBuilder builder = new StringBuilder();

        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());

            for (Comment comment : comments) {
                builder.append(comment.toString());
            }

            builder.append(String.format("%s", COMMENTS_HEADER));
        }

        return builder.toString();
    }
}
