package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;


public class TruckImpl extends VehicleBase implements Truck {

    private final static String CAPACITY_FIELD = "Weight Capacity";

    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK);
        setWeightCapacity(weightCapacity);
    }

    @Override
    public int getWeightCapacity() { return weightCapacity; }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  %s: %dt",CAPACITY_FIELD,getWeightCapacity());
    }


    //There are 2 variants of Weight Capacity used in output. So to be fully equally it needs some editing
    private void setWeightCapacity(int weightCapacity) {
        Validator.ValidateIntRange(weightCapacity, ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY,
                String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        (CAPACITY_FIELD.substring(0,1) + CAPACITY_FIELD.substring(1).toLowerCase()),
                        ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY));

        this.weightCapacity = weightCapacity;
    }

}
