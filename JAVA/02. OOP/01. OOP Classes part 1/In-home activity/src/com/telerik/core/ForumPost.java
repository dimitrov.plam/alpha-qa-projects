package com.telerik.core;

public class ForumPost {
    String author;
    String text;
    int upVotes;

    public ForumPost(String author, String text, int upVotes){
        this.author = author;
        this.text = text;
        this.upVotes = upVotes;
    }
    public ForumPost(String author, String text){
        this.author = author;
        this.text = text;
    }
}

