package com.telerik.core;

import java.time.LocalDate;

public class BoardItem {
    private static final int MIN_TITLE_LENGTH = 5;
    private static final int MAX_TITLE_LENGTH = 30;
    private String title;
    private LocalDate dueDate;
    private Status status;

    public enum Status{
        OPEN, TODO,INPROGRESS, DONE, VERIFIED;

        public static final Status[] allStatuses = values();

        public Status prev() {
            if (ordinal() > 0){
                return allStatuses[(ordinal() - 1) % allStatuses.length];
            }
            else return allStatuses[ordinal()];
        }

        public Status next() {
            if (ordinal()< (allStatuses.length -1)) {
                return allStatuses[(this.ordinal() + 1) % allStatuses.length];
            }
            else return allStatuses[ordinal()];
        }
    }

    public BoardItem(String title, LocalDate dueDate){
        this(title, dueDate, Status.values()[0]);
    }

    public BoardItem(String title, LocalDate dueDate, Status status){
        setTitle(title);
        setDueDate(dueDate);
        setStatus(status);
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (title == null){
            throw new IllegalArgumentException("Title can't be null");
        }
        if (title.length()< MIN_TITLE_LENGTH || title.length() > MAX_TITLE_LENGTH){
            throw new IllegalArgumentException("Title name must be between 5 and 30 characters");
        }

        this.title = title;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        if (dueDate.isBefore(LocalDate.now())){
            throw new IllegalArgumentException("Due date can't be in the past");
        }
        this.dueDate = dueDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status revertStatus(){
        return status = status.prev();
    }
    public  Status advanceStatus(){
        status = status.next();
        return status;

    }
    public String viewInfo(){
        String statusStr = null;
        switch (status.ordinal()){
            case 0:{
                statusStr = "Open";
                break;
            }
            case 1:{
                statusStr = "To do";
                break;
            }
            case 2: {
                statusStr = "In Progress";
                break;
            }
            case 3: {
                statusStr = "Done";
                break;
            }
            case 4: {
                statusStr = "Verified";
                break;
            }
            default: break;
        }

        return String.format("'%s', [%s | %s]", title, statusStr, dueDate.toString());

    }

    public static void main(String[] args) {
        BoardItem item = new BoardItem("Registration doesn't work", LocalDate.now().plusDays(2));
        System.out.println(item.status);
        item.advanceStatus();
        System.out.println(item.status);
        item.advanceStatus();
        System.out.println(item.status);
        item.revertStatus();
        System.out.println(item.status);
        item.revertStatus();
        System.out.println(item.status);
        item.revertStatus();
        System.out.println(item.status);
        item.revertStatus();
        System.out.println(item.status);
        item.revertStatus();
        System.out.println(item.status);

        System.out.println(item.viewInfo());

    }

}

