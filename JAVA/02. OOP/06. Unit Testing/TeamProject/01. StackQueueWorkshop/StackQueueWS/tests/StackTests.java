

import com.telerikacademy.Stack;
import com.telerikacademy.StackImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class StackTests {
    
    private StackImpl<Integer> testStack;
    
    @BeforeEach
    public void before() {
        this.testStack = new StackImpl<>();
    }


    @Test
    public void StackImpl_ShouldImplementStackInterface () {

        Assertions.assertTrue(testStack instanceof Stack);

    }

    @Test
    public void constructor_ShouldCreateEmptyStack(){
        Assertions.assertEquals(0, testStack.size());
    }

    @Test
    public void constructor_ShouldCreateUserDefinedStack(){

        testStack = new StackImpl<>(Arrays.asList(22,33,44));

        Assertions.assertEquals(new StackImpl<>(Arrays.asList(22,33,44)), testStack);
    }

    @Test
    public void push_ShouldUpdateSize_WhenSingleElementAdd(){
        testStack.push(5);

        Assertions.assertEquals(1,testStack.size());
    }

    @Test
    public void pop_ShouldThrow_WhenEmptyStack(){

        Assertions.assertThrows(IllegalArgumentException.class, () -> testStack.pop());
    }

    @Test
    public void pop_ShouldReturnDeletedElement() {

        testStack = new StackImpl<>(Arrays.asList(22,33,44));

        Assertions.assertEquals(44, testStack.pop());
    }

    @Test
    public void pop_ShouldUpdateSize() {

        testStack = new StackImpl<>(Arrays.asList(1,2));
        testStack.pop();

        Assertions.assertEquals(1, testStack.size());
    }

    @Test
    public void peek_ShouldThrow_WhenEmptyStack(){

        Assertions.assertThrows(IllegalArgumentException.class, () -> testStack.peek());
    }

    @Test
    public void peek_ShouldReturnLastElement () {

        testStack = new StackImpl<>(Arrays.asList(1,2,3));

        Assertions.assertEquals(3, testStack.peek());
    }


    @Test
    public void size_ShouldReturnNumberOfElements () {

        testStack = new StackImpl<>(Arrays.asList(22,33,44));

        Assertions.assertEquals(3, testStack.size());

    }

    @Test
    public void isEmpty_ShouldReturnTrue_WhenStackIsEmpty (){

        Assertions.assertTrue(testStack.isEmpty());
    }

    @Test
    public void isEmpty_ShouldReturnFalse_WhenStackIsFilled (){

        testStack = new StackImpl<>(Arrays.asList(22,33,44));

        Assertions.assertFalse(testStack.isEmpty());
    }
}
