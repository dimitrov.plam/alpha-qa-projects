

import com.telerikacademy.Queue;
import com.telerikacademy.QueueImpl;
import com.telerikacademy.StackImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class QueueTests {
    
    private Queue<Integer> testQueue;
    
    @BeforeEach
    public void before() {
        testQueue = new QueueImpl<Integer>();
    }


    @Test
    public void QueueImpl_ShouldImplementQueueInterface(){

        Assertions.assertTrue(testQueue instanceof Queue);
    }

    @Test
    public void constructor_ShouldCreateEmptyQueue(){

        Assertions.assertEquals(0,testQueue.size());
    }

   /* @Test
    public void constructor_ShouldCreatUserDefinedQueue(){}*/

    @Test
    public void offer_ShouldUpdateSize(){
        testQueue.offer(33);

        Assertions.assertEquals(1,testQueue.size());
    }

    @Test
    public void poll_ShouldThrow_WhenEmptyQueue(){

        Assertions.assertThrows(IllegalArgumentException.class, ()-> testQueue.poll());
    }

    @Test
    public void poll_ShouldReturnDeletedElement(){

        testQueue = new QueueImpl<>(Arrays.asList(22,33,44));

        Assertions.assertEquals(22,     testQueue.poll());
    }

    @Test
    public void poll_ShouldUpdateSize(){

        testQueue = new QueueImpl<>(Arrays.asList(22,33,44));
        testQueue.poll();

        Assertions.assertEquals(2, testQueue.size());
    }

    @Test
    public void peek_ShouldThrow_WhenEmptyQueue(){

        Assertions.assertThrows(IllegalArgumentException.class, ()-> testQueue.peek());
    }

    @Test
    public void peek_ShouldReturnFirstElement(){

        testQueue = new QueueImpl<>(Arrays.asList(22,33,44));

        Assertions.assertEquals(22, testQueue.peek());
    }

    @Test
    public void size_ShouldReturnNumberOfElements(){

        testQueue = new QueueImpl<>(Arrays.asList(22,33,44));

        Assertions.assertEquals(3, testQueue.size());
    }

    @Test
    public void isEmpty_ShouldReturnTrue_WhenQueueIsEmpty(){

        Assertions.assertTrue(testQueue.isEmpty());
    }

    @Test
    public void isEmpty_ShouldReturnFalse_WhenQueueIsFilled(){

        testQueue = new QueueImpl<>(Arrays.asList(22,33,44));

        Assertions.assertFalse(testQueue.isEmpty());
    }
}
