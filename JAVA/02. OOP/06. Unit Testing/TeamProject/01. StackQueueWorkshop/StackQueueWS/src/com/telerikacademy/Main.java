package com.telerikacademy;

import java.util.Arrays;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here

        StackImpl<Integer> testStack2 = new StackImpl();
        StackImpl<Integer> testStack1 = new StackImpl(Arrays.asList(1,2,3,4,5));

        testStack2.push(5);

        System.out.println(testStack1);
        System.out.println(testStack2);

    }
}
