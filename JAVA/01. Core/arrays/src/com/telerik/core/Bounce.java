//package com.telerik.core;

import java.util.Scanner;

public class Bounce {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String [] input = sc.nextLine().split(" ");
        int rowLength = Integer.parseInt(input[0]);
        int colLength = Integer.parseInt(input[1]);
        long[][] matrix = fillMatrixWithPowerOfTwo(rowLength,colLength);
        boolean rightDown = true;
        boolean rightUp = false;
        boolean leftDown = false;
        boolean leftUp = false;
        int row = 0;
        int col = 0;
        long path = 0;
        while (rightDown || rightUp || leftDown || leftUp){
            if (rightDown){
                path = path + matrix[row][col];
                if ((row == rowLength - 1) || (col == colLength-1)){
                    rightDown =false;
                    if((row == rowLength -1) && !(col == colLength-1)){
                        rightUp = true;
                        row--;
                        col++;
                    }
                    if (!(row == rowLength -1) && (col == colLength-1)){
                        leftDown = true;
                        row++;
                        col--;
                    }
                }
                else{
                    row++;
                    col++;
                }
            }
            if (rightUp){
                path = path + matrix[row][col];
            }
            if (leftDown){
                path = path + matrix[row][col];
            }
            if (leftUp){

            }
        }

            System.out.print(path);



    }
    public static  long[][] fillMatrixWithPowerOfTwo(int row, int col){
        long [][] array = new long[row][col];
        long filler = 1;
        for (int i = 0; i < row; i++) {
            if (i>0){
                filler = filler * 2;
            }
            for (int j = 0; j < col; j++) {
                if (j > 0){
                    array[i][j] =array[i][j-1]* 2;
                }
                else{
                    array[i][j] = filler;
                }
            }
        }
        return array;
    }
    public  static void printIntegerMatrix(long[][] array){
        //int[][] array = new int[row][col];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
