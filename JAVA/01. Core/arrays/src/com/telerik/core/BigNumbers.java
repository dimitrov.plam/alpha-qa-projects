package com.telerik.core;

import java.util.ArrayList;
import java.util.Scanner;

public class BigNumbers {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String [] input = sc.nextLine().split(" ");
        String[] firstDigit = sc.nextLine().split(" ");
        String[] secondDigit = sc.nextLine().split(" ");
        ArrayList<String> result = new ArrayList<>();


        sumOfTwoArrays(firstDigit, secondDigit, result);

        System.out.println(String.join(" ", result));

    }

    public static void sumOfTwoArrays(String[] firstDigit, String[] secondDigit,
                                      ArrayList<String> result) {
        int digit;
        boolean carrying = false;
        for (int i = 0; i < Math.max(firstDigit.length, secondDigit.length); i++) {

            if (i<Math.min(firstDigit.length, secondDigit.length)) {
                if (carrying){
                digit = sumOfTwoStrings(firstDigit[i], secondDigit[i]) +1;
                }
                else{
                    digit = sumOfTwoStrings(firstDigit[i], secondDigit[i] );
                }
                if (digit>=10){
                    carrying = true;
                    digit = digit % 10;
                }
                else{
                    carrying = false;
                }
            }
            else {
                if (i>= firstDigit.length){
                    if (carrying){
                        digit = Integer.parseInt(secondDigit[i]) + 1;
                    }
                    else{
                        digit = Integer.parseInt(secondDigit[i]);
                    }
                    if (digit>=10){
                        carrying = true;
                        digit = digit % 10;
                    }
                    else{
                        carrying = false;
                    }
                }
                else{
                    if (carrying){
                        digit = Integer.parseInt(firstDigit[i]) + 1;
                    }
                    else{
                        digit = Integer.parseInt(firstDigit[i]);
                    }
                    if (digit>=10){
                        carrying = true;
                        digit = digit % 10;
                    }
                    else{
                        carrying = false;
                    }
                }
            }
            result.add(Integer.toString(digit));
        }
        if (carrying){
            result.add("1");
        }
    }

    public static int sumOfTwoStrings(String  str1, String str2){
        return (Integer.parseInt(str1) +Integer.parseInt(str2));
    }
    
    
}
