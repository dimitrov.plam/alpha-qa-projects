package com.telerik.core;

import java.util.Scanner;

public class oldBounce {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String [] input = sc.nextLine().split(" ");
        int rowLength = Integer.parseInt(input[0]);
        int colLength = Integer.parseInt(input[1]);
        long[][] matrix = fillMatrixWithPowerOfTwo(rowLength,colLength);
        boolean rightDown = true;
        boolean rightUp = false;
        boolean leftDown = false;
        boolean leftUp = false;
        int row = 0;
        int col = 0;
        long path = 0;
        if (rowLength==1 ||colLength==1){
            System.out.print(1);

        }
        else{
        while (rightDown || rightUp || leftDown || leftUp){
            if (rightDown){
                path = path + matrix[row][col];
                row++;
                col++;
                if ((row==rowLength) || (col==colLength)){
                    if ((row==rowLength) && (col==colLength)) {
                        rightDown = false;
                    }
                    else
                    if (row == rowLength){
                        rightDown =false;
                        rightUp = true;
                        row= row-2;
                    }
                    else{
                        rightDown = false;
                        leftDown = true;
                        col=col - 2;
                    }
                }

            }
            if (rightUp){
                path = path + matrix[row][col];
                row--;
                col++;
                if ((row<0) || (col==colLength)){
                    if ((row<0) && (col==colLength)){
                        rightUp = false;
                    }
                    else
                    if (col== colLength){
                        rightUp =false;
                        leftUp= true;
                        col= col-2;
                    }
                    else{
                        rightUp = false;
                        rightDown = true;
                        row = row + 2;

                    }
                }
            }
            if (leftDown){
                path = path + matrix[row][col];
                row++;
                col--;
                if ((row == rowLength) || (col<0)){
                    leftDown = false;
                    if ((row == rowLength) && (col<0)){

                    }
                    else
                    if (row == rowLength){
                        rightUp = true;
                        row = row - 2;
                    }
                    else{
                        rightDown = true;
                        col = col +2;
                    }
                }
            }
            if (leftUp){
                path = path + matrix[row][col];
                row--;
                col--;
                if ((row<0) || (col<0)){
                    leftUp = false;
                    if ((row<0) && (col<0)){

                    }
                    else
                    if (row<0){
                        leftDown = true;
                        row = row +2;
                    }
                    else{
                        rightUp= true;
                        col = col+2;
                    }
                }
            }
        }
        System.out.print(path);
        }


    }
    public static  long[][] fillMatrixWithPowerOfTwo(int row, int col){
        long [][] array = new long[row][col];
        long filler = 1;
        for (int i = 0; i < row; i++) {
            if (i>0){
                filler = filler * 2;
            }
            for (int j = 0; j < col; j++) {
                if (j > 0){
                    array[i][j] =array[i][j-1]* 2;
                }
                else{
                    array[i][j] = filler;
                }
            }
        }
        return array;
    }
    public  static void printIntegerMatrix(long[][] array){
        //int[][] array = new int[row][col];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}