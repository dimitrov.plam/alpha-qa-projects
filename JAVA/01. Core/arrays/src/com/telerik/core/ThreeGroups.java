package com.telerik.core;

import java.util.ArrayList;
import java.util.Scanner;

public class ThreeGroups {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String [] input = sc.nextLine().split(" ");
        ArrayList<String> zeroRemainder = new ArrayList<>();
        ArrayList<String> oneRemainder = new ArrayList<>();
        ArrayList<String> twoRemainder = new ArrayList<>();
        for (int i = 0; i < input.length; i++) {
            int remainder = (Integer.parseInt(input[i])) % 3;
            switch (remainder){
                case 0 : zeroRemainder.add(input[i]);
                         break;
                case 1 : oneRemainder.add(input[i]);
                        break;
                case 2 : twoRemainder.add(input[i]);
                        break;
                default: break;
            }
        }
        System.out.println(String.join(" ", zeroRemainder));
        System.out.println(String.join(" ", oneRemainder));
        System.out.println(String.join(" ", twoRemainder));
    }
}
