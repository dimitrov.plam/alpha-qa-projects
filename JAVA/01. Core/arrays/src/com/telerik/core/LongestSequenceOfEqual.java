package com.telerik.core;

import java.util.Scanner;

public class LongestSequenceOfEqual {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = Integer.parseInt(sc.nextLine());
        int[] array = new int[N];
        for (int i = 0; i < N; i++) {
            array[i]  = Integer.parseInt(sc.nextLine());
        }
        boolean hasSequence = false;
        int counter =1;
        int maxSequence = 1;
        for (int i = 0; i < N - 1; i++) {
            if (array[i] == array[i+1]){
                hasSequence = true;
                counter++;
                if (counter > maxSequence){
                    maxSequence = counter;
                }
            }
            else{
                hasSequence = false;
            }

            if (!hasSequence){
                counter = 1;
            }
        }
        System.out.print(maxSequence);

    }
}
