package com.telerik.core;

import java.util.Scanner;

public class SpiralMatrix {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = Integer.parseInt(sc.nextLine());
        int[][] matrix = new int[N][N];

        boolean goRight = true;
        boolean goDown = false;
        boolean goLeft = false;
        boolean goUp = false;
        boolean direction;

        int counter = 1;
        int row = 0;
        int col = 0;

        int leftEnd = 0;
        int lowerEnd = 0;
        int rightEnd = 0;
        int upperEnd = 0;

        while (counter <= N*N){
            if (goRight){
                matrix[row][col] = counter;
                counter++;
                if((col + 1)!= (matrix[row].length - rightEnd)){
                    goRight = false;
                    goDown = true;
                    row++;
                    rightEnd++;

                }
                else{
                    col++;
                }

            }
            if (goDown){
                matrix[row][col] = counter;
                counter++;
                if ((row + 1) == (matrix.length - lowerEnd)){
                    goDown = false;
                    goLeft = true;
                    col--;
                    lowerEnd++;

                }
                else{
                    row++;
                }
            }
            if (goLeft){
                matrix[row][col] = counter;
                counter++;
                if ((col) == (leftEnd)){
                    goLeft = false;
                    goUp = true;
                    row--;
                    leftEnd++;

                }
                else{
                    col--;
                }
            }
            if (goUp){
                matrix[row][col] = counter;
                counter++;
                if ((row) == (upperEnd)){
                    goUp = false;
                    goRight = true;
                    col++;
                    upperEnd++;
                }
                else{
                    row--;
                }
            }
        }
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
