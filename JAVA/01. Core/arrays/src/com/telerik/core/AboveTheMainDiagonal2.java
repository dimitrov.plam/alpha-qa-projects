package com.telerik.core;


import java.util.Scanner;

public class AboveTheMainDiagonal2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = Integer.parseInt(sc.nextLine());

        //initialize square matrix of numbers, formed by powers of 2
        long[][]  matrix = fillMatrixWithPowerOfTwo(N,N);
        long sum= 0;

        // 2 foor loops for walking above main diagonal
        for (int i = 0; i < N; i++) {
            for (int j = i; j < N; j++) {
                sum = sum + matrix[i][j];
            }
        }
        //printIntegerMatrix(matrix);
        System.out.print(sum);
    }
    public static  long[][] fillMatrixWithPowerOfTwo(int row, int col){
        long [][] array = new long[row][col];
        long filler = 1;
        for (int i = 0; i < row; i++) {
            if (i>0){
                filler = filler * 2;
            }
            for (int j = 0; j < col; j++) {
                if (j > 0){
                    array[i][j] =array[i][j-1]* 2;
                }
                else{
                    array[i][j] = filler;
                }
            }
        }
        return array;
    }
    public  static void printIntegerMatrix(long[][] array){
        //int[][] array = new int[row][col];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
