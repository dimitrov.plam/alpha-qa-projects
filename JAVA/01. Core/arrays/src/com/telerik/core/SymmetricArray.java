package com.telerik.core;

import java.util.ArrayList;
import java.util.Scanner;

public class SymmetricArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N =  Integer.parseInt(sc.nextLine());

        for (int i = 0; i < N; i++) {
            String[] input = sc.nextLine().split(" ");
            int[] array = new int[input.length];
            for (int j = 0; j < input.length; j++) {
                array[j] = Integer.parseInt(input[j]);
            }
            boolean isSymmetric = true;
            for (int j = 0; j < array.length/2; j++) {

                if (array[j] != array[array.length- 1 - j]){
                    isSymmetric = false;
                    break;
                }
            }
            if (isSymmetric){
                System.out.println("Yes");
            }
            else{
                System.out.println("No");
            }
        }
    }
}
