# Test Design Technics 
#Homework

## Topic

| Test Case Title                                                              | Priority |       Technique      |
|------------------------------------------------------------------------------|:--------:|:--------------------:|
| Create topic with meaningful Title and   some content text                   |  Prio 1  |          EQ          |
| Create a valid topic with category and   tag                                 |  Prio 2  | Classification trees |
| Create topic with empty Title and Post   field                               |  Prio 2  |          EQ          |
| Create topic with 4 chars in Title field   and 9 chars in Post field         |  Prio 2  |          BVA         |
| Create topic with 256 chars in Title   field                                 |  Prio 3  |          BVA         |
| Create topic with 100 chars as a word in   Title field and some content text |  Prio 3  |          EQ          |
| Create topic with some text in Title   and  3200 chars in Post field         |  Prio 3  |          BVA         |
| Delete topic                                                                 |  Prio 2  |  Exploratory testing |

## Comment

| Test Case Title                                           | Priority |      Technique      |
|-----------------------------------------------------------|:--------:|:-------------------:|
| Create comment with some text                             |  Prio 1  |          EQ         |
| Create comment with no content                            |  Prio 2  |          EQ         |
| Delete commment                                           |  Prio 2  | Exploratory testing |
| Create comment with bold numered list                     |  Prio 3  |   Pairwise testing  |
| Create comment with preformated bulleted   list           |  Prio 3  |   Pairwise testing  |
| Create comment with link in blockquotes                   |  Prio 3  |   Pairwise testing  |
| create comment with whole post quote and   italic content |  Prio 3  |   Pairwise testing  |
| Create    comment with iploaded image and emoji           |  Prio 3  |   Pairwise testing  |
| Create    comment with inserted date                      |  Prio 3  |   Pairwise testing  ||

## Notification

| Test Case Title                                 | Priority | Technique           |
|-------------------------------------------------|----------|---------------------|
| Check for notification after creating   topic   | Prio 3   | Exploratory testing |
| Check for notification after creating   comment | Prio 3   | Exploratory testing |
|                                                 |          |                     |