# Test Plan
## <span style="color:#0066ff">Test Plan Identifier </span>
Master Test plan for Telerik Academy Forum

URL: https://forum.telerikacademy.com/

*version: 1.0* 

## <span style="color:#0066ff"> Summary

The purpose of this document is to outline the implementation of the main Agile Testing principles in the "Telerik Academy forum" solution as well as the tools and techniques used to assert the product quality. 

## <span style="color:#0066ff"> Scope
- The site will be tested on **System level**
- Only **"User"** actor role will be tested

### <span style="color:darkgreen"> Features to be tested
- Forum Topic
- Forum Comment
- Messages
- Submenu for sorting and visualising topics

### <span style="color:darkgreen"> Features NOT to be tested
- Earning badges
- profile edit
- profile activity
- notification

## <span style="color:#0066ff"> Testing types

1. Exploratory testing
2. Functional testing
3. Performance testing
4. Stress amd Load testing
5. Security testing

## <span style="color:#0066ff"> Schedule

| Feature  | Est. time | Additional time | Start date | End date |
|:---------|----------:|-----------------|------------|----------|
| Topic    | 1 day     |                 | 22.04.2021 |23.04.2021|
| Comment  | 1 day     |                 | 23.04.2021 |24.04.2021|
| Messages | 1 day     |                 | 26.04.2021 |27.04.2021|
| Messages | 1 day     |                 | 27.04.2021 |28.04.2021|


## <span style="color:#0066ff"> Exit Criteria

### <span style="color:darkgreen"> Test Coverage
- All Blocking or Critical priority tests passed
- All High priority tests passed
- 95% of other tests passed
- At least 95% of code coverage
- All major functional flows are executed successfully with various inputs and are working fine

### <span style="color:darkgreen"> Deadlines
- Project Deadline or Test Finish deadline is reached

### <span style="color:darkgreen"> Budget
- Complete Testing Budget is exhausted