package Pages.StageTelerikForum;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage_PageFactory {

    private static final String TITLE_NAME = "Simpe Topic Creation Test";

    private WebDriver driver;

    public HomePage_PageFactory(WebDriver webdriver) {
        driver= webdriver;
        PageFactory.initElements(webdriver, this);
    }

    @FindBy(xpath = "//button[contains(@class,'login-button')]")
    WebElement loginButton;

    @FindBy(xpath = "//button[@id='create-topic']")
    WebElement createNewTopicButton;

    @FindBy(xpath = "//div[contains(@class,'title-input')]/div[contains(@class,'popup-tip bad')]")
    WebElement emptyTitlePopup;

    @FindBy(xpath = "//div[@class='d-editor-textarea-wrapper  ']/div[contains(@class,'popup-tip bad')]")
    WebElement emptyPostPopup;

    @FindBy(xpath = "//input[@id='reply-title']")
    WebElement titleInput;

    @FindBy(xpath = "//textarea[contains(@class,'d-editor-input ember-text-area')]")
    WebElement postInput;

    @FindBy(linkText = TITLE_NAME)
    WebElement titleNameInForum;


    public void CreateEpmtyTopic(){
        createTopicButton.click();

    }


}
