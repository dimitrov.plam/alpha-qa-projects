package Pages.StageTelerikForum;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;


public class LogInPage_PageFactory {

    public static final String EMAIL = "buggs.bunnies.telerik@gmail.com";
    public static final String PASSWORD = "Buggs-Bunn1es";

    private WebDriver driver;

    public LogInPage_PageFactory(WebDriver webdriver){
        driver = webdriver;
        PageFactory.initElements(webdriver, this);
    }




    @FindBy(xpath = "//input[@name='Email']")
    WebElement emailField;

    @FindBy(xpath = "//input[@name='Password']")
    WebElement passwordField;
    @FindBy(xpath = "//button[@id='next']")
    WebElement signInButton;

    @FindBy(xpath = "//p[.='Wrong email or password.']")
    WebElement errorMessage;


    public void signInWithEmailAndPassword(){
        emailField.sendKeys(EMAIL);
        passwordField.sendKeys(PASSWORD);
        signInButton.click();

    }
}
