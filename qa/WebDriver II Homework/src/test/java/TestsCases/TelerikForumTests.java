package TestsCases;

import Pages.StageTelerikForum.HomePage_PageFactory;
import Pages.StageTelerikForum.LogInPage_PageFactory;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;


public class TelerikForumTests {
    private static final String HOME_URL = "https://stage-forum.telerikacademy.com/";

    private WebDriver webdriver;

    @BeforeClass
    public static void classSetup(){
        WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();


    }

    @Before
    public void setup(){
        // Chrome
        /*ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito")*/;

        webdriver = new ChromeDriver();

        webdriver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);


        webdriver.get(HOME_URL);
    }
    @Before
    public void loginInToForum(){
        WebElement logInButton = webdriver.findElement(By.className("d-button-label"));
        logInButton.click();

        LogInPage_PageFactory logInPage = new LogInPage_PageFactory(webdriver);
        logInPage.signInWithEmailAndPassword();


    }

    @After
    public void tearDown(){
        webdriver.quit();
    }

    @Test
    public void CreateEmptyTopic(){
        HomePage_PageFactory homePage = new HomePage_PageFactory(webdriver);
homePage.createTopicButton
        WebElement newTopicButton = webdriver.findElement(By.id("create-topic"));
        newTopicButton.click();

        WebElement createTopicButton = webdriver.findElement(By.xpath("//button[@aria-label='Create Topic']"));
        createTopicButton.click();

        WebElement titleIsrequired = webdriver.findElement(By.xpath("//div[@class='title-and-category with-preview']/div[1]/div"));
        WebElement postCannotBeEmpty = webdriver.findElement(By.xpath("//div[@class='d-editor-button-bar']/div[3]"));

        Assert.assertTrue("Title is required message was not displayed.", titleIsrequired.isDisplayed());
        Assert.assertTrue("Post can't be empty message was not displayed.", postCannotBeEmpty.isDisplayed());

    }
}
